package com.devcamp.slitstringapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SlitstringapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SlitstringapiApplication.class, args);
	}

}
